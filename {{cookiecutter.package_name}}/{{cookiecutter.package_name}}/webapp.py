import os
import urllib

import bglogs
import cherrypy
from ws4py.websocket import WebSocket
from ws4py.server.cherrypyserver import WebSocketPlugin, WebSocketTool
from bgweb import BGWeb
from bgweb.jobs import JobManagerError, JobNotFoundError, MaxStoredJobsError, MaxConcurrentJobsError, \
    {% if cookiecutter.anonymous_jobs == 'Yes' %}MaxAnonymousJobsError, {% endif %}AuthorizationError, JobSubmissionDisabledError
from bgweb.profile import BGProfileError, BGProfileUpdateError
{%- if cookiecutter.anonymous_jobs == 'Yes' %}
from bgweb.users import UsersManager
{%- else %}
from bgweb.users import BasicUsersManager as UsersManager
{%- endif %}
from bgweb.tools.authentication import RegisterUsersToFileListener
from bgweb.tools.authentication.auth0 import Auth0
from bgweb.tools.authentication.utils import require_signin
from bgweb.utils import download as file_download


logger = bglogs.get_logger(__name__)


class WebApp(BGWeb):

    def __init__(self, env, conf, static_dirs, jobs_manager):

        self.env = env
        super().__init__(conf, static_dirs)

        self.jobs_manager = jobs_manager

        self.websocket_domain = conf['websocket_domain']

        self.env.globals['path_info'] = lambda: cherrypy.request.path_info

        # Enable websockets for the jobs
        self.server_conf['/ws'] = {
            'tools.websocket.on': True,
            'tools.websocket.handler_cls': WebSocket,
            'tools.gzip.on': False
        }
        WebSocketPlugin(cherrypy.engine).subscribe()
        cherrypy.tools.websocket = WebSocketTool()

    def setup(self, global_conf):
        self.app_name = global_conf['apps']['name']
        self.env.globals['app_name'] = self.app_name

        self.server_conf['/']['error_page.default'] = self.error  # Custom error page handler

        # Add favicon
        self.server_conf['/favicon.ico'] = {'tools.staticfile.on': True,
                                            'tools.staticfile.filename': os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'static', 'images', 'group', 'favicon.ico')
                                            }

        url = urllib.parse.urljoin(global_conf['server']['public_url'], self.mount_point)
        self.url = url[:-1] if url.endswith('/') else url  # remove trailing '/'

        # Enable the tools for the session and auth in the root
        self.server_conf['/']['tools.sessions.on'] = True
        self.server_conf['/']['tools.authentication.on'] = True

        auth0_conf = global_conf['security']['auth0']

        self.auth = Auth0(auth0_conf,
                          app_url=self.url,
                          login_path='/signin',
                          logout_path='/signout',
                          force_login_function=self.force_login,
                          logout_redirect='home?msg={}'.format(urllib.parse.quote_plus('Logged out'))
                          )

        cherrypy.tools.authentication = self.auth

        if global_conf['security'].get('registered_users', None) is not None:
            cherrypy.tools.authentication.add_listener(
                RegisterUsersToFileListener(global_conf['security']['registered_users']))

        self.api_href = global_conf['apps']['api']['mount_point']
        mount_point = '' if self.api_href == '/' else self.api_href
        mount_point = mount_point[1:] if mount_point.startswith('/') else mount_point
        self.api_url = urllib.parse.urljoin(global_conf['server']['public_url'], mount_point)

        self.users_manager = UsersManager(self.auth, self.app_name)
        self.env.globals['getUser'] = self.users_manager.get_registered_user

    def force_login(self, msg=None):
        """
        Function for the authentication tool

        Returns:
            str. The loging page code without changing the URL
        """
        return self.env.get_template('login.html').render(msg=msg)

    def error(self, status, message, traceback, version):
        """Custom error page"""
        logger.info('Sending user to error page %s -- %s -- %s' % (status, message, traceback))
        return self.env.get_template('error.html').render(code=status, msg=message, trace=traceback)

## -- Basic pages
    @cherrypy.expose
    def index(self):
        """
        Redirect to home
        """
        raise cherrypy.HTTPRedirect("home")

    @cherrypy.expose
    def home(self, msg=None):
        """
        Home page containing the main description and the examples.

        See home.html

        Args:
            msg (str): optional message that will be shown as a warning

        """
        return self.env.get_template('home.html').render(
            msg=msg,
            example_jobs=self.jobs_manager.get_examples()
        )

    @cherrypy.expose
    def about(self):
        """
        About page.

        See about.html
        """
        return self.env.get_template('about.html').render()

    @cherrypy.expose
    def faq(self):
        """
        Frquently asked questions page.

        See faq.html
        """
        return self.env.get_template('faq.html').render()

    @cherrypy.expose
    def forbidden(self, msg=None):
        """
        See forbidden.html

        Args:
            msg (str): Optional message displayed as info

        Returns:

        """
        return self.env.get_template('forbidden.html').render(msg=msg)

## -- Jobs related methods
    @cherrypy.expose

    {%- if cookiecutter.anonymous_jobs != 'Yes' %}
    @require_signin()
    {%- endif %}
    def analysis(self, msg=None):
        """
        Analysis submit page.
        This page forces registered users to accepts the terms and update their profile
        when terms are not yet accepted.
        For anonymous users, the must accept the terms in a checkbox before being able to submit a job

        Args:
            msg (str): optional message that will be shown as a warning

        See analysis_form.html

        """
        terms = False

        user = self.users_manager.get_user()
        if user is None:
            raise cherrypy.HTTPError(500, 'Unknown user')

        # Do not show the page if the use cannot submit more jobs
        try:
            self.jobs_manager.can_create_new_job(user)
        except MaxStoredJobsError:
            raise cherrypy.HTTPError(403, 'You have reached the maximum number of jobs permitted. Please, delete one before submitting a new job.')
        except MaxConcurrentJobsError:
            raise cherrypy.HTTPError(403, 'You have reached the maximum number of concurrent jobs permitted. Please, wait for the other jobs you have submitted.')
        except JobSubmissionDisabledError:
            raise cherrypy.HTTPError(503, 'Job submission is currently not available due to maintenance reasons. Please, try again later.')
        {%- if cookiecutter.anonymous_jobs == 'Yes' %}
        except MaxAnonymousJobsError:
            return self.force_login(
                msg="You have reached the maximum number of jobs allowed without registration. "
                    "Please, register to keep using this site")
        {%- endif %}

        profile = user.profile
        if profile is not None:
            terms = profile.terms_accepted(self.app_name)

        is_admin = True if user is not None and user.id in self.jobs_manager.admins else False

        return self.env.get_template('analysis_form.html').render(
            msg=msg,
            terms_accepted=terms,
            is_admin=is_admin
        )

    @cherrypy.expose
    def results(self, msg=None, id=None):
        """
        Show job results (if id is provided) or a list with all users jobs.

        Args:
            msg (str): optional message that will be shown as a warning
            id (str): ID of the job (optional)

        See job_results.html or results.html
        """
        user = self.users_manager.get_user()
        if id is None:
            {%- if cookiecutter.anonymous_jobs == 'Yes' %}
            user_jobs = self.jobs_manager.get_user_jobs(user)
            if user.isanonymous:
                jobs = {}
                anonymous_jobs = user_jobs[user.id]
            else:
                jobs = user_jobs[user.id]
                anonymous_jobs = user_jobs[user.anonymous_alias]
            return self.env.get_template('results.html').render(
                msg=msg,
                jobs=jobs,
                anonymous_jobs=anonymous_jobs
            )
            {%- else %}
            if user is None:
                return self.force_login()
            else:
                return self.env.get_template('results.html').render(
                    msg=msg,
                    jobs=self.jobs_manager.get_user_jobs(user)[user.id]
                )
            {%- endif %}
        else:
            try:
                job = self.jobs_manager.get_job(user, id)
            except JobNotFoundError:
                # raise cherrypy.HTTPRedirect("forbidden?msg=Job not found")
                return self.env.get_template('job_results.html').render(
                    msg='Job not found',
                    id=id
                )
            except AuthorizationError:
                raise cherrypy.HTTPRedirect("forbidden")

            is_example = self.jobs_manager.is_example(user, id)
            is_public = self.jobs_manager.is_public(user, id)
            user_owns_job = self.jobs_manager.is_owner(user, id)
            is_salvable = True if self.jobs_manager.has_anonymous_owner(user, id) and not user.isanonymous else False
            is_admin = True if user is not None and user.id in self.jobs_manager.admins else False

            return self.env.get_template('job_results.html').render(
                msg=msg,
                websocket=self.websocket_domain,
                lines=job.lines,
                status=job.status,
                job=job,
                id=id,
                user_owns_job=user_owns_job,
                user_is_admin=is_admin,
                is_example=is_example,
                is_public=is_public,
                is_salvable=is_salvable
            )

    @cherrypy.expose


    {%- if cookiecutter.anonymous_jobs != 'Yes' %}
    @require_signin()
    {%- endif %}
    def submit(self, name_1, name_2, name_3, name_4, name_7, name_8, license='no', name_5=None, name_6='default'):

        # FIXME set the parameters

        terms = True if license == 'yes' else False
        if not terms:
            raise cherrypy.HTTPError(401, 'Please accept the terms before submitting a job')

        user = self.users_manager.get_user()
        try:
            job_metadata, job_folder = self.jobs_manager.create_job(user)
        except MaxStoredJobsError:
            raise cherrypy.HTTPError(403, 'You have reached the maximum number of jobs permitted. Please, delete one before submitting a new job.')
        except MaxConcurrentJobsError:
            raise cherrypy.HTTPError(403, 'You have reached the maximum number of concurrent jobs permitted. Please, wait for the other jobs you have submitted.')
        except JobSubmissionDisabledError:
            raise cherrypy.HTTPError(503, 'Job submission is currently not available due to maintenance reasons. Please, try again later.')
        {%- if cookiecutter.anonymous_jobs == 'Yes' %}
        except MaxAnonymousJobsError:
            raise cherrypy.HTTPError(403, 'You cannot create more anonymous jobs')
        {%- endif %}

        # FIXME fill metadata, save files in the job_folder...
        # FIXME load the command
        command = ''
        job_id = self.jobs_manager.launch_job(command, job_metadata)
        raise cherrypy.HTTPRedirect("results?id={}".format(job_id))

    @cherrypy.expose
    {%- if cookiecutter.anonymous_jobs != 'Yes' %}
    @require_signin()
    {%- endif %}
    def remove(self, id):
        """
        Remove a job. Redirect to results page.

        Args:
            id (str): Job ID

        """
        try:
            self.jobs_manager.remove(self.users_manager.get_user(), id)
        except JobManagerError as e:
            raise cherrypy.HTTPRedirect("forbidden?msg={}".format(e.message))
        else:
            raise cherrypy.HTTPRedirect("results?msg=Job {} has been removed".format(id))

    @cherrypy.expose(alias='update')
    {%- if cookiecutter.anonymous_jobs != 'Yes' %}
    @require_signin()
    {%- endif %}
    def update_metadata(self, id, action, value):
        """
        Update job information (metadata). Then redirects to results.

        Args:
            id (str): job ID
            action (str): indicates what is going to be updated
            value (str): value of the update

        ==============  =============   =============================
        action          value           meaning
        ==============  =============   =============================
        title           text            update title
        description     text            update description
        add / remove    user email      give / remove reading rights
        add / remove    **anonymous**   give / revoke public access
        add / remove    **example**     mark / unmark as example
        ==============  =============   =============================
        """
        # assuming only metadata update is required
        try:
            # do action related stuff
            if action == 'title':
                self.jobs_manager.change_metadata(self.users_manager.get_user(), id, 'title', str(value))
                message = 'Title updated'
            elif action == 'description':
                self.jobs_manager.change_metadata(self.users_manager.get_user(), id, 'description', str(value))
                message = 'Description updated'
            elif action == 'add':
                user = str(value).lower()
                if user == 'example':
                    self.jobs_manager.mark_as_example(self.users_manager.get_user(), id)
                    message = "Now this analysis is an example."
                elif user == 'anonymous':
                    self.jobs_manager.mark_as_public(self.users_manager.get_user(), id)
                    message = "Now this analysis is public, anyone with the URL {}/results?id={} can access and browse it.".format(
                        self.url, id
                    )
                else:
                    self.jobs_manager.share_with(self.users_manager.get_user(), id, user)
                    message = "User {0} now has read access. We do not send automatic emails to other users. Provide {0} this URL {1}/results?id={2} .".format(
                        user, self.url, id
                    )
            elif action == 'remove':
                user = str(value).lower()
                if user == 'example':
                    self.jobs_manager.unmark_as_example(self.users_manager.get_user(), id)
                    message = "This job is no longer an example"
                elif user == 'anonymous':
                    self.jobs_manager.unmark_as_public(self.users_manager.get_user(), id)
                    message = "The public access has been revoked."
                else:
                    self.jobs_manager.unshare_with(self.users_manager.get_user(), id, user)
                    message = "User {} removed from shared users".format(user)

        except JobManagerError as e:
            raise cherrypy.HTTPRedirect("results?id={}&msg={}".format(id, e.message))
        else:
            raise cherrypy.HTTPRedirect("results?id={}&msg={}".format(id, message))

    {%- if cookiecutter.anonymous_jobs == 'Yes' %}

    @cherrypy.expose
    @require_signin()
    def save(self, id):
        """
        Change the owner of a job from anonymous to the registered user
        Args:
            id (str): job id
        """
        user = self.users_manager.get_user()
        try:
            self.jobs_manager.change_owner(user, id, user.id)
        except JobManagerError as e:
            raise cherrypy.HTTPRedirect("results?id={}&msg={}".format(id, e.message))
        else:
            raise cherrypy.HTTPRedirect("results?id={}&msg={}".format(id, 'Job saved'))
    {%- endif %}

    @cherrypy.expose
    def download(self, id, file):
        """
        Download result files

        Args:
            id (str): job ID
            file (str): desired job file

        """
        try:
            directory = self.jobs_manager.get_directory(self.users_manager.get_user(), id)  # check if the job exists and we have permissions
        except JobNotFoundError:
            raise cherrypy.HTTPError(404, 'Job not found')
        except AuthorizationError:
            raise cherrypy.HTTPRedirect("forbidden?msg={}".format('Your are not authorized to access this job. Please request permissions to the owner.'))
        except JobManagerError as e:
            raise cherrypy.HTTPError(500, e.message)

        download_file = os.path.join(directory, file)

        try:
            return file_download(download_file)
        except FileNotFoundError:
            raise cherrypy.HTTPError(404, 'File {} not found for job {}'.format(file, id))

    @cherrypy.expose
    def ws(self, id):
        """
        Called once by analysis_task to add a websocket handler for the
        current job (called once by each user).

        Args:
            id (str): job ID

        """
        self.jobs_manager.add_handler(id, cherrypy.request.ws_handler)

    @cherrypy.expose
    def status(self, id, line, host=None, port=None):
        """
        Called by the server to pass the messages to the clients using the
        websockets.

        Args:
            id (str): job ID
            line(str): message

        """
        self.jobs_manager.send_message(id, "{}".format(line))

## -- user profile
    @cherrypy.expose
    @require_signin()
    def update_profile(self, to, institution, newsletter='no', terms='no'):
        user = self.users_manager.get_user()
        profile = None if user is None else user.profile
        if profile is not None:
            try:
                if institution == '':
                    institution = None
                if institution != profile.institution:
                    profile.update_institution(institution)
                newsletter = True if newsletter == 'yes' else False
                if newsletter != profile.newsletter:
                    if newsletter:
                        profile.accept_newsletter()
                    else:
                        profile.reject_newsletter()
                terms = True if terms == 'yes' else False
                current_terms = profile.terms_accepted(self.app_name)
                if terms != current_terms:
                    if terms:
                        profile.accept_terms(self.app_name)
                    else:
                        profile.reject_terms(self.app_name)
            except BGProfileError as e:
                raise cherrypy.HTTPError(503,
                                         'Sorry, something went wrong while updating your profile. Please, try later. If the error persist, contact us. Error: {}'.format(
                                             e.message))
        raise cherrypy.HTTPRedirect(to)

## -- rest_api page related
    @cherrypy.expose
    def rest_api(self):
        mount_point = '' if self.mount_point == '/' else self.mount_point + '/'
        mount_point = mount_point[1:] if mount_point.startswith('/') else mount_point
        return self.env.get_template('rest_api.html').render(
            generate_token_url=mount_point + 'generate_token',
            apiurl=self.api_url,
            apihref=self.api_href,
            file_ext='tsv'  # FIXME extension of the download file from the REST API
        )

    @cherrypy.expose
    @require_signin()
    def generate_token(self):
        user = self.users_manager.get_user()
        user_profile = None if user is None else user.profile
        if user_profile is not None:
            if not user_profile.terms_accepted(self.app_name):
                raise cherrypy.HTTPError(401, 'Please accept the terms before submitting a job')
            try:
                user_profile.generate_rest_api_token()
                return user_profile.rest_api_token
            except BGProfileUpdateError as e:
                raise cherrypy.HTTPError(500, e.message)
        else:
            logger.warning('Missing user profile')
            raise cherrypy.HTTPError(503, 'User profile not found')

    @cherrypy.expose(alias="token.js")
    def tokenjs(self):
        """

        Returns:
            str. JavaScript code used to log in/out the users from the
            navigation bar

        """
        cherrypy.response.headers['Content-Type'] = 'text/javascript'
        cherrypy.response.headers['Cache-Control'] = 'no-cache'
        return self.env.get_template('token.js').render()

## -- Authentication related scripts that should be not cached
    @cherrypy.expose(alias="user.js")
    def userjs(self):
        """

        Returns:
            str. JavaScript code used to log in/out the users from the
            navigation bar

        """
        cherrypy.response.headers['Content-Type'] = 'text/javascript'
        cherrypy.response.headers['Cache-Control'] = 'no-cache'
        return self.env.get_template('user.js').render()

    @cherrypy.expose
    def login(self, page=None):
        if page is not None:
            cherrypy.session['auth_redirect_url'] = page
        raise cherrypy.HTTPRedirect(self.auth.get_login_url())

    @cherrypy.expose
    def logout(self, page=None):
        if page is not None:
            cherrypy.session['auth_redirect_url'] = page
        raise cherrypy.HTTPRedirect(self.auth.get_logout_url())

## -- Management methods
    @cherrypy.expose
    @require_signin()
    def submissions(self, status):
        """
        Enable/disable jobs submissions
        """
        user = self.users_manager.get_user()

        if status == 'on':
            try:
                self.jobs_manager.enable_jobs_submission(user)
            except AuthorizationError:
                raise cherrypy.HTTPError(403, 'Invalid permissions')
            return 'ENABLED'
        elif status == 'off':
            try:
                self.jobs_manager.disable_jobs_submission(user)
            except AuthorizationError:
                raise cherrypy.HTTPError(403, 'Invalid permissions')
            return 'DISABLED'
        else:
            raise cherrypy.HTTPError(501, 'Status {} not valid'.format(status))
