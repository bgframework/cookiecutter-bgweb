// Template for generating dynamically the content
// of the user button in the nav-bar
// and the profile form

{% set user = getUser() %}
{% set user_profile = user.profile or none%}


$(document).ready(function() {
    {% if user_profile is none %}
        hide_token();
    {% else %}
        show_token_button();
        {% if user_profile.rest_api_token is not none %}
            show_token('{{user_profile.rest_api_token}}');
        {% endif %}
    {% endif %}
});
