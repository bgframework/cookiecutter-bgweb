from os import path

import bglogs
import cherrypy
from bgconfig import BGConfig
from jinja2 import Environment, FileSystemLoader
from bgweb.jobs import {% if cookiecutter.anonymous_jobs == 'Yes' %}JobsManagerAllowingAnonymous as JobsManager{% else %}JobsManager{% endif %}
from bgweb.server import BGServer

from {{cookiecutter.package_name}}.apiapp import RESTAPIApp
from {{cookiecutter.package_name}}.webapp import WebApp


if __name__ == '__main__':

    project_directory = path.dirname(path.dirname(path.abspath(__file__)))

    config_file = path.join(project_directory, 'conf/system.conf')
    config_template = path.join(project_directory, 'conf/system.conf.template')
    config_spec = path.join(project_directory, 'conf/system.confspec')
    conf = BGConfig(config_template, config_file=config_file, config_spec=config_spec)

    bglogs.configure(debug=True)
    bglogs.configure('{{cookiecutter.package_name}}', debug=False)
    bglogs.configure(name='bgweb', debug=False)
    bglogs.configure(name='cherrypy.access', debug=False)
    bglogs.configure(name='cherrypy.error', debug=False)

    static_dirs = ['css', 'images', 'js', 'libs']
    static_base_dir = path.abspath(path.join(project_directory, 'static'))
    static_dirs_dict = {}
    for dir in static_dirs:
        static_dirs_dict['/' + dir] = path.join(static_base_dir, dir)

    env = Environment(loader=FileSystemLoader(path.join(project_directory, 'templates')))

    jobs_manager = JobsManager(conf['scheduler'], conf['apps']['name'])

    web_app = WebApp(env, conf['apps']['web'], static_dirs_dict, jobs_manager)
    web_app.setup(conf)

    rest_app = RESTAPIApp(conf['apps']['api'], jobs_manager)
    rest_app.setup(conf)

    {%- if cookiecutter.anonymous_jobs == 'Yes' %}

    # Create a background job to remove expired anonymous jobs
    removejobs = cherrypy.process.plugins.BackgroundTask(3600, jobs_manager.clean_anonymous_jobs)
    removejobs.start()
    {%- endif %}

    server = BGServer(conf['server'])

    server.add_app(web_app)
    server.add_app(rest_app)

    bglogs.debug('Starting {{cookiecutter.package_name}}')

    server.run()

