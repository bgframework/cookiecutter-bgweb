
cookiecutter-bgweb
==================

Template for BBGLab websites::

   |- <package_name>/
   |
   |  |- bin/
   |  |  |- web.py
   |
   |  |- conf/
   |  |  |- system.conf.template
   |  |  |- system.confspec
   |
   |  |- static/
   |  |  |- css/
   |  |  |  |- ...
   |  |  |
   |  |  |- images/
   |  |  |  |- ...
   |  |  |
   |  |  |- js/
   |  |  |  |- ...
   |  |  |
   |  |  |- libs/
   |  |  |  |- ...
   |  |  |
   |
   |  |- templates/
   |  |  |- ...
   |
   |  |- <package_name>/
   |  |  |- __init__.py
   |  |  |- apiapp.py
   |  |  |- webapp.py
   |
   |- requirements.txt


If ``conda`` is found, this project will ask whether you want to create
a **conda environment** with the dependencies installed or not.


Usage
-----

This is a template for the |cc|_ project.

To use it, install |cc|_ and call:

.. code:: bash

   cookiecutter cookiecutter-bgweb


.. important::

   If it is the first time you use this package, |cc|_
   can download it from our repository:

   .. code:: bash

      cookiecutter https://bitbucket.org/bgframework/cookiecutter-bgweb.git


Authors
-------

This tool has been developed by the
`Barcelona Biomecial Genomics Lab <https://bbglab.irbbarcelona.org/>`_


.. |cc| replace:: cookiecutter
.. _cc: https://github.com/audreyr/cookiecutter

