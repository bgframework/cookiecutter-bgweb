FROM bbglab/miniconda:4.4.10

# Configure environment
ENV PYTHONPATH=/app

# Copy all files 
COPY . /app

# Install requirements as root
USER root
RUN conda install -y --file=/app/requirements.txt
RUN chown -R $USER: /app

# Run as normal user
USER $USER
WORKDIR /app
EXPOSE 8080
CMD ["python", "bin/web.py"]
