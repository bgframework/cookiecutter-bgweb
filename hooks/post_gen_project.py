import os
import shutil
from os import path

import click

CONDA_CMD = '{conda} create -n {name} python={version} jinja2 cherrypy ws4py bgdata bgconfig bgweb bglogs --yes'


def is_executable(executable):
    return False if shutil.which(executable) is None else True


def which(program):
    fpath, fname = path.split(program)
    if fpath:
        if is_executable(program):
            return program
    else:
        for path_ in os.environ["PATH"].split(os.pathsep):
            exe_file = path.join(path_, program)
            if is_executable(exe_file):
                return exe_file


conda_path = which('conda')

if conda_path is not None:
    click.echo('Conda found in {}'.format(conda_path))
    if click.confirm('Do you want to create a conda environment for the project?', default=True):

            env_name = click.prompt('Give a name to your environment', default='{{cookiecutter.package_name}}')
            python_version = click.prompt('Indicate the Python version', default=None)

            cmd = CONDA_CMD.format(conda=conda_path, name=env_name, version=python_version)

            print(cmd)

            return_code = os.system(cmd)

            if return_code != 0:
                print('ERROR: Cannot create conda environment')
                # sys.exit(1)  Do not exit, is not crucial
